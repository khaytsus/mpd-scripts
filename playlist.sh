#!/bin/bash

# Add a single song into the mpd queue based on the playlist desired

# This script can be customized to add additional playlist types or 
# different criteria for your playlists

# Load this before grabbing arguments
scriptDir="$(dirname "$(readlink -f "$0")")"
if [ -e ${scriptDir}/env.sh ]; then
    source ${scriptDir}/env.sh
fi

# Get the current mpd version so we can see if it supports searching by stickers
mpdversion=`mpc ${mpcOptions} version | cut -f 2 -d "."`

# Sanity check that mpd is available
if [ $? != 0 ]; then
    echo "$0 mpd version returned: [$?]"
    exit
fi

# If mpd version is less than 23, we still need nc
#if [ ${mpdversion} -lt 23 ]; then
    nc="/bin/nc"
    if [ ! -x ${nc} ]; then
        echo "${nc} does not exist"
        exit
    fi
#fi

# We need shuf to pick a random line from our cache files
shuf="/bin/shuf"
if [ ! -x ${shuf} ]; then
    echo "${shuf} does not exist"
fi

playlist=$1

if [ "${playlist}" == "" ]; then
    echo "$0 [playlist]"
    exit
fi

function logline
{
    # Uncomment this out to get some debug data in the logfile
    return
    logfile=${HOME}/.playlist.log
    date=`date +"%Y-%m-%d_%H-%M-%S"`
    echo "$date - $*" >> ${logfile}
}

# Star rating values are doubled, so 3 stars = 6, 3.5 stars = 7, etc

# Flag so we know that we matched a file and can continue on
matched=0

# How many minutes our cache files are good for
cacheAge="60"

# Get a local copy of our library so we can pick a file from it
function getSongs
{
    # Do something to get stickers in and use them, tbd
    stickers=$*
    getcache="0"
    # Make sure cacheFile is defined
    if [ "${cacheFile}" != "" ]; then
        # First see if our file exists and is not empty and if so,
        # do we need to refresh it
        if [ -e ${cacheFile} ] && [ -s ${cacheFile} ]; then
            epoch=`date +%s`
            fileStat=`stat -L --format %Y ${cacheFile}`
            fileAge=$((epoch - fileStat))
            maxAge=$((cacheAge * 60))
            logline "fileage: ${fileAge} maxage: ${maxAge}"
            if [ ${fileAge} -gt ${maxAge} ]; then
                # Cache file is old, recreating
                getcache="1"
            fi
        else
            # Cache file does not exist, creating
            getcache="1"    
        fi

        logline "getcache: ${getcache}"
        # If we need to, update the cache 
        if [ ${getcache} -eq 1 ]; then
            # mpd less than 23 we have to use nc
            if  [ $mpdversion -lt 23 ]; then
                echo "${query}" | ${nc} ${host} ${port} | grep "file: " > ${cacheFile}
            else
                # Otherwise, we can use mpc directly
                #echo "mpc search here when ready"
                echo "${query}" | ${nc} ${host} ${port} | grep "file: " > ${cacheFile}
            fi
        fi

    else
        echo "No cache file specified, cannot proceed"
        exit
    fi
}

# Play a pair of songs if we are going to play either of them
function pairedSongs
{
    uri=$1
    # Split our string on |, between pairs of songs.  The paired songs
    # themselves are divided with a #
    OIFS=$IFS
    IFS='|'
    for song in ${paired} ; do
        # See if our current song is in this pair
        match=`echo "${song}" | grep ${uri}`
        if [ "${match}" != "" ]; then
            # If so, reset IFS and add both songs to the queue
            IFS=$OIFS
            songone=`echo ${song} | cut -f 1 -d '#'`
            songtwo=`echo ${song} | cut -f 2 -d '#'`
            mpc ${mpcOptions} add "${songone}"
            mpc ${mpcOptions} add "${songtwo}"
            return
        fi
    done

    # If we found no matches, reset IFS and add the single URI to the queue
    IFS=$OIFS
    mpc ${mpcOptions} add "${uri}"
}

# Filter results and add to queue
function addSong
{
    line="${1}"
    filters="${2}"
    # Clean out the "file: " prefix from the line
    uri=`echo $line | sed -e 's/^file: //'`
    
    # Filter anything we don't want to listen to based on the passed-in filters
    match=`mpc ${mpcOptions} search "((file == \"${uri}\") ${filters})"`
    
    # Get this tracks title and make sure it isn't in our playlist already
    matchname=`mpc ${mpcOptions} -f "%title%" search "(file == \"${match}\")"`
    matchtest=`echo "${currentPlayList}" | grep -F "${matchname}"`

    # If we get a filename back and it's not already in, add it to our queue
    if [ "${match}" != "" ] && [ "${matchtest}" == "" ]; then
        pairedSongs "${uri}"
        ((matched++))
    fi
}

# Example which gets all songs from our library, then excludes anything that is
# Classical
function noclassical
{
    # Cached file of all songs in our playlist
    cacheFile="$HOME/.mpd-all.txt"
    # What query do we pass the mpd protocol
    query='listall'
    # What to filter out of results
    filters="AND (genre != 'Classical')"
    # Populate our cache file with filenames to pick from
    getSongs
    while [ ${matched} == 0 ]; do
        # Pick a random line from our cache file
        line=`${shuf} -n 1 ${cacheFile}`
        addSong "${line}" "${filters}"
    done

    done=1
}

# My typical listening mix of stuff I like and stuff I haven't rated
function threestars
{
    filters="AND (genre != 'Classical') AND (genre != 'Opera') AND (genre != 'Country') AND (artist != 'Hearts of Space') AND (comment != 'Exclude')"
    
    # Logic slightly different here as we're mixing two cache files together
    while [ ${matched} == 0 ]; do
        # Mix up two cache files, 80% 3+ stars and 20% 0 stars
        random=$(( ( RANDOM % 100 )  + 1 ))
        if [ ${random} -gt 20 ]; then
            cacheFile="$HOME/.mpd-threestars.txt"
            # 3+ stars = rating 6, so > 5
            query='sticker find "song" "" rating > 5'
        else
            cacheFile="$HOME/.mpd-zerostars.txt"
            query='sticker find "song" "" rating = 0'
        fi
        getSongs
        line=`${shuf} -n 1 ${cacheFile}`
        addSong "${line}" "${filters}"
    done

    done=1
}

# My Favorite songs
function fourstars
{
    cacheFile="$HOME/.mpd-fourstars.txt"
    query='sticker find "song" "" rating > 7'
    filters="AND (genre != 'Classical')"
    getSongs
    while [ ${matched} == 0 ]; do
        line=`${shuf} -n 1 ${cacheFile}`
        addSong "${line}" "${filters}"
    done

    done=1
}

# Songs to discover that I haven't rated
function zerostars
{
    cacheFile="$HOME/.mpd-zerostars.txt"
    query='sticker find "song" "" rating = 0'
    filters="AND (genre != 'Classical') AND (genre != 'Opera') AND (genre != 'Country') AND (artist != 'Hearts of Space') AND (comment != 'Exclude')"
    getSongs
    while [ ${matched} == 0 ]; do
        line=`${shuf} -n 1 ${cacheFile}`
        addSong "${line}" "${filters}"
    done

    done=1
}

# In the future, support my own stickers, such as trippy, chill, etc, as I
# populate them

# Get our current queue so we can avoid duplicates
currentPlayList=`mpc ${mpcOptions} playlist`

${playlist}

if [ "${done}" != "1" ]; then
    echo ""
    echo "Unknown playlist $playlist"
fi
