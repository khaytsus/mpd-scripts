#!/bin/bash

# Maintain our queue continuously

# Location of the script to add files to the queue
scriptDir="$(dirname "$(readlink -f "$0")")"
queueScript="${scriptDir}/addqueue.sh"

while [ 1 ]; do
    # Pull data from external script if exists
    if [ -e ${scriptDir}/env.sh ]; then
        source ${scriptDir}/env.sh
    else
        "env.sh is required to define playlists, exiting"
        exit
    fi
    version=`mpc ${mpcOptions} version`
    if [ "$?" == "0" ]; then
        ${queueScript} ${playlist} ${queueSize} ${oldSongs}
        # If you prefer you can have mpc delay the script until the current song has
        # completed before it loops.
        play_status=`mpc ${mpcOptions} status | grep '\[playing\]\|\[paused\]' | cut -f 2 -d '[' | cut -f 1 -d ']'`
        if [ "$play_status" = "playing" ]; then
            mpc ${mpcOptions} current --wait &> /dev/null
        fi
    fi
    # Just in case sleepTime isn't defined, sleep
    if [ "${sleepTime}" == "" ]; then
        sleep 2s
    else
        sleep ${sleepTime}s
    fi
done