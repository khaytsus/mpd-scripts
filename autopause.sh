#!/bin/bash

# Script to track if media is playing and automatically pause mpd and
# resume mpd when the media stops if it was playing.

# Manually resuming mpd while media is playing will allow playing both 
# and mpd will continue playing after the media has stopped.

# This script does not do anything to mpd unless media is playing.

# This should be started at desktop login as it will loop forever looking
# for events.  It does look to make sure it's not already running.

# Edit mediaApps in env.sh to modify which media apps are being watched

# Make sure playerctl is installed
command -v playerctl &> /dev/null
if [ ${?} != 0 ]; then
    echo "playerctl could not be found"
    exit 1
fi

# Populate mpcOptions and mediaApps from env.sh
scriptDir="$(dirname "$(readlink -f "$0")")"
if [ -e "${scriptDir}/env.sh" ]; then
    source "${scriptDir}/env.sh"
fi

# Make sure our variable is populated
if [ "${mediaApps}" == "" ]; then
    echo "mediaApps variable in env.sh is empty, exiting"
    exit 1
fi

# Check to make sure this script isn't already running
status=$(pgrep -fc "${0}")
if [ "${status}" -gt 1 ]; then
    echo "Already running, exiting"
    exit
fi

# Track if we should resume mpd
resumempd='0'

# Track the timstamp of when media or mpd started playing so we know
# if mpd was started after media to avoid getting immediately paused
mpdts='0'
mediats='0'

# Loop forever so we catch events quickly
while true; do

    # Track if mpd is currently playing
    mpdplaying='0'
    mpdstatus=$(mpc ${mpcOptions} status '%state%')
    if [ "${mpdstatus}" == 'playing' ]; then
        mpdplaying='1'
        if [ "${mpdts}" -eq 0 ]; then
            mpdts=$(date +%s)
        fi
    else
        # Reset timestamp if not playing
        mpdts='0'
    fi

    # Track if any media player is playing
    mediaplaying='0'

    # Loop through media apps to determine if any are currently playing
    for mediaapp in ${mediaApps}; do
        mediastatus=$(playerctl status -sp "${mediaapp}")
        if [ "${mediastatus}" == 'Playing' ]; then
            mediaplaying='1'
        fi
    done

    # Check for other media/programs/logic using extraMedia in env.sh
    extraMedia

    # If media is playing set mediats, otherwise reset to zero
    if [ "${mediaplaying}" -eq 1 ]; then
        if [ "${mediats}" -eq 0 ]; then
        mediats=$(date +%s)
        fi
    else
        mediats='0'
    fi

    #echo "mediats: [${mediats}] mpdts: [${mpdts}] mediaplaying: [${mediaplaying}]"
    # Reset resumempd to 0 if mpd is playing and media is paused
    if [ "${mpdplaying}" -eq 1 ] && [ "${mediaplaying}" -eq 0 ]; then
        resumempd='0'
    fi

    # If media is playing and mpd is also playing, pause it
    if [ "${mediaplaying}" -eq 1 ] && [ "${mpdplaying}" -eq 1 ] && \
       [ "${resumempd}" -eq 0 ]; then
        # Only pause if mpd's timestamp is older than the media timestamp;
        # ie: it was already playing when media started
        if [ "${mpdts}" -le "${mediats}" ]; then
            mpc ${mpcOptions} -q pause
            # Resume after media stops playing
            resumempd='1'
        fi
    fi

    # If mpd is paused and resumempd is set, resume mpd
    if [ "${mediaplaying}" -eq 0 ] && [ "${mpdplaying}" -eq 0 ] && \
       [ "${resumempd}" -eq 1 ]; then
        mpc ${mpcOptions} -q play
        # Reset resumempd so to avoid conflict with controlling mpd
        # without media being involved
        resumempd='0'
    fi

    sleep 1s
done
