#!/bin/bash

cmd=$1
value=$2

if [ -e ${scriptDir}/env.sh ]; then
    source ${scriptDir}/env.sh
fi

# Adjust this to however much you want the volup/voldown to adjust
voldelta="5"

# Set rating; if rating already matches, bump up half a star
# Rating values are double that of star value, so rating=6 is 3 stars
function rating
{
	rating=$1
	# Rating passed in, set it
	if [ "${rating}" != "" ]; then
		rating=`echo "(${rating} * 2) / 1" | bc`
		file=`mpc ${mpcOptions} current -f '%file%'`
		oldrating=`mpc ${mpcOptions} sticker "${file}" get rating | cut -f 2 -d "="`
		if [ ${rating} == ${oldrating} ] && [ ${rating} -ne 10 ]; then
			rating=$((rating + 1))
		fi
		mpc ${mpcOptions} sticker "${file}" set rating $rating
        oldratingline=`echo "scale=1;(${oldrating} / 2)" | bc`
        ratingline=`echo "scale=1;(${rating} / 2)" | bc`
        # If we can come up with a half star, eh, maybe some day
        #ratingstars=`echo "(${rating} / 2)" | bc`
	    line="Rating changed to ${ratingline} (was ${oldratingline})"
        #stars=$(printf '⭐%.0s' $(seq 1 ${ratingstars}))
        echo "${line}"
        notify-send "${line}" --icon=starred -t 3000
	else
		file=`mpc ${mpcOptions} current -f '%file%'`
		rating=`mpc ${mpcOptions} sticker "${file}" get rating | cut -f 2 -d "="`
        ratingline=`echo "scale=1;(${rating} / 2)" | bc`
		line="Current rating is ${ratingline}"
        echo ${line}
        notify-send "${line}" --icon=starred -t 3000
	fi
}

# Adjust the volume
function volume
{
	arg=$1
	origvol=`mpc ${mpcOptions} volume | cut -f 2 -d " " | cut -f 1 -d "%"`
	if [ ${arg} == "up" ]; then
		vol=$((origvol + voldelta))
	else
		vol=$((origvol - voldelta))
	fi
	echo "Changing from ${origvol} to ${vol}"
	mpc ${mpcOptions} -q volume ${vol}
}

status=$(mpc status | grep play)

case ${cmd} in
  # Music controls
  next)
	mpc ${mpcOptions} next
    ;;
  prev)
	mpc ${mpcOptions} prev
    ;;
  stop)
	mpc ${mpcOptions} stop
    ;;
  play)
	mpc ${mpcOptions} play
    ;;
  pause)
	#mpc ${mpcOptions} toggle
    if [ -n "$status" ]
        then
           mpc pause >> /dev/null
        else
           mpc play >> /dev/null
    fi
    ;;
  # Utilities
  rating)
	rating ${value}
    ;;
  volup)
	volume up
	;;
  voldown)
	volume down
	;;
  *)
	echo "$0 cmd [value]"
	exit
	;;
esac