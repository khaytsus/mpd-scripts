#!/bin/bash

# Maintain a queue of songs for mpd, adding as necessary to keep the queue
# of old songs and total songs at the requested amount

# Probably not much reason to edit this script

# Set path to script that generates the playlist
scriptDir="$(dirname "$(readlink -f "$0")")"
playlistScript="${scriptDir}/playlist.sh"

# Load this before grabbing arguments
if [ -e ${scriptDir}/env.sh ]; then
    source ${scriptDir}/env.sh
fi

playlist=$1
queueSize=$2
oldSongs=$3

if [ "${playlist}" == "" ] | [ "${queueSize}" == "" ] | [ "${oldSongs}" == "" ]; then
    echo "$0 [playlist] [queueSize] [oldSongs]"
    exit
fi

# Get our full playlist
currentPlayList=`mpc ${mpcOptions} playlist`

# Sanity check that mpd is available
if [ $? != 0 ]; then
    echo "$0 mpd playlist returned: [$?]"
    exit
fi

play_status=`mpc ${mpcOptions} status | grep '\[playing\]\|\[paused\]' | cut -f 2 -d '[' | cut -f 1 -d ']'`
if [ "$play_status" = "playing" ];then
    # Get our current song
    currentSong=`mpc ${mpcOptions} current`
    # What position is the current song in the queue
    # Issue I need to fix here sometime; duplicate entries in the list
    # Worked around with grep -m 1 for now, but likely would hose population
    currentPos=`echo "${currentPlayList}" | grep -F -m 1 -n "${currentSong}" | cut -f 1 -d ":"`
else
    currentPos=0
fi

# Clean up any old songs at the top of the queue first until our current
# position is oldSongs + 1 (leaving N oldSongs above it)
while [ ${currentPos} -gt $((oldSongs + 1)) ]; do
    # Get rid of the first song, refresh our list
    mpc ${mpcOptions} del 1
    # Refresh our location in the queue
    currentPlayList=`mpc ${mpcOptions} playlist`
    currentSong=`mpc ${mpcOptions} current`
    currentPos=`echo "${currentPlayList}" | grep -F -n "${currentSong}" | cut -f -1 -d ":"`
done

# Evaluate how big is our queue is now
currentSize=`echo "${currentPlayList}" | wc -l`

# If our playlist was empty, start playing when we're done
if [ "${currentPlayList}" == "" ]; then
    play=1
    # And set currentSize to 0, since it is
    currentSize=0
fi

# If the queue is shorter than desired, add songs
if [ ${currentSize} -le ${queueSize} ]; then
    # How many songs do we need to add to fill up our playlist
    toAdd=$((queueSize - currentSize))
    # Add as many files as we need
    for (( i=1; i<=${toAdd}; i++ )); do
        ${playlistScript} ${playlist}
    done
fi

# Think about if I want this to do this or not, have to ponder it more
if [ "${play}" == "1" ]; then
    mpc ${mpcOptions} -q play
fi