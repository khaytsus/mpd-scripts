#!/bin/bash

# Get or set a sticker for the current playing song

operation=$*

usage ()
{
	echo "$0 [get/set/list/delete/find] [sticker] (value)"
	exit
}

if [ "${1}" == "" ]; then
	usage
fi	

scriptDir="$(dirname "$(readlink -f "$0")")"
if [ -e ${scriptDir}/env.sh ]; then
    source ${scriptDir}/env.sh
fi
current=`mpc ${mpcOptions} current -f '%file%'`
name=`mpc ${mpcOptions} current`

if [ "${1}" == "get"  ] || 
   [ "${1}" == "list" ]; then
	echo "${name}"
	mpc ${mpcOptions} sticker "${current}" ${operation}
elif [ "${1}" == "set"    ] || 
     [ "${1}" == "delete" ]; then
 	echo "${name}: ${operation}"
	mpc ${mpcOptions} sticker "${current}" ${operation}
elif [ "${1}" == "find" ]; then
	mpc ${mpcOptions} sticker "" ${operation}
else
	usage
fi
