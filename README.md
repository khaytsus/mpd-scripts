# mpd scripts

This is a set of bash scripts to control mpd to maintain a queue of a defined playlist type.  This effectively adds smart playlists to mpd outside of any other client.  I use it to add a mix of songs of a specific rating and a mix with no rating, but none which are of a specific genre.  You can really use it do anything you like, but something with ratings is my primary use case.

Note that mpd has no inherent idea of 'rating', this uses the 'stickers' function in mpd and uses the a key name of 'rating' primarily because this seems to be a reasonably common sticker name for rating.  But this isn't strictly required by any means, you can set up any playlist criteria you like in playlist.sh as described below.

If you happen to be migrating from Amarok there is one script in the amarok directory that can help migrate ratings out of Amarok into the mpd sticker database.

This repo also contains a script to automatically fade the volume and pause when the computer monitors are off/screen is locked, resume playing after waking.

# Requirements
 * mpd 
 * mpc 
 * nc (netcat)
 * shuf (gnu-utils)
 * playerctl (optional; for autopause.sh)

The playlist.sh script needs nc (netcat) to get data from the mpd server and shuf (from gnu coreutils) to pick a random line from the result mpd returns.

# Queuing Songs

In order to have these scripts queue songs for you, you'll need to at least update the playlist.sh script to define a playlist criteria you prefer (see the specific section below for more details) and update the queueloop.sh script to tell it to use that playlist.  That should work well enough to build and maintain an mpd queue for you.  From there you can customize playlists.sh to your hearts content to make mpd play exactly what you want.

# Queue Scripts

As an overall note for the queue scripts, every effort has been made to minimize excessive usage of mpc etc, although it still does quite a bit of querying to find songs etc.  There are no 'writes' that happen during the queueing.  No last played dates, no sticker updates of any kind.

## queueloop.sh 

This script is intended to be ran probably at login time or on the mpd servers so that it is constantly refilling the mpd queue with songs.  Using the settings you put in the env.sh script that is used by this script for personalized settings you can define the queue you want to build, how big that queue is, and how many old songs remain in the queue (think of it as a delayed consume mode).

This script is pretty simple and just runs forever, looking to see if mpd is playing and if so, waits until the current song is over and then adds another song to the queue.  There is likely no need to modify this file.

This script calls env.sh for settings, and addqueue.sh to do the queue maintenance bits.

## env.sh 

You can adjust the playlist, queue size, old songs, host and port for the mpd server, etc on the fly by modifying the env.sh file.  And if your mpd is on a remote server, you can define the hostname and port in this file.  You may also set any other options you'd want mpc to use for every innovocation of the mpc command througout this script.

You will need to edit the env.sh to set your playlist requirements as well as your mpd server hostname and port.  You can change this file while queueloop.sh is running and it will pick up the changes, such as changing to a different type of playlist, making your queue longer, etc.  New songs will reflect these changes but if you prefer the changes to take place immediately you could execute `mpc clear` or clear it in your client as appropriate.

## addqueue.sh 

This is a bit of a utility script and likely does not need to be edited.  Taking input from queueloop.sh this script basically evaluates the current queue situation and makes it match whatever parameters you've set in queueloop.sh.  If it needs to remove old songs, it will do that, if it needs to add more songs, it will do that.  It will not make the queue smaller, with the exception of just removing old songs.

Keep in mind that there is no real way to tell if a song has actually been played, only the position of the current song relative to the current queue.  So if you skip to a song at the end of the queue for example, it will remove the other songs in the queue above it as 'old'.

This script calls playlist.sh any time it needs to add a song to the queue.

## playlist.sh 

This is the script that has smart brains in it.  In order customize it for your playlist preferences, you'll have to edit this file add add your own preferred playlist type, such as play songs that aren't in the "Classical" genre.  I have an example function called noclassical that does that which is well commented to explain what each step is doing.

In order to get a list of songs that match any playlist type, the script creates a 'cache' file using the mpd server protocol language to ask for a list of files that match the query, store this in a file, then grabs a random line from that file.  Unfortunately the mpd protocol does not support doing this directly so we do it in an intermediate file.

So in each playlist you want to define, you first define the name of the cache file to keep, the query to use (see the [mpd protocol page](https://www.musicpd.org/doc/html/protocol.html)), then do any additional filtering.  So for the "noclassical" playlist, we get all songs mpd has currently, then ask mpc to query if this file is NOT Classical.  If that is true it will give us the filename back as confirmation, then we tell mpc to add that file to the queue.

The example playlists check to see if the song found is already in the current queue and if so, it won't add it.  This stops duplicate songs in the existing queue but still does not make any attempt to only play songs that have not been recently played.

I also have a other example playlists called threestars, fourstars, and zerostars.

threestars is my primary playlist generator and generates about 80% songs with a rating of 3 stars or above, 20% with no rating at all.  This gives me a mix of songs I like (3+ stars means I want to generally hear the song whenever) but also mixes in some which have not been ranked at all, giving me the opportunity to rank them.  In order to get this list, I ask the mpd server for stickers rather than all files like in the "noclassical" example, ie:  `sticker find "song" "" rating > 5` if I want to find a rating of 6 or better.

zerostars basically lets me discover music I need to tag in some way and is the same as threestars, without the 80/20 mix.

fourstars returns songs that are only 3.5 stars or greater; basically my favorite songs.  So like above, we would ask for rating > 6 to find those with a rating of 7 or better.

# Audio fade/pausing

Also included is a script which will fade the volume and pause mpd once it reaches a certain volume level so that when your computer is idle the music plays for a while and then stops.  If you want mpd playing 24/7, this script isn't for you.  If you basically want music playing while you're sitting at the computer and you want it to fade when the computer is idle, it might be useful.  This assumes the computer is not going to sleep, only sitting idle.  mpd itself can handle a computer going to sleep, so if your computer goes to sleep when you're not using it this script might also not be useful for you.

There are a few things you can tweak in it to adjust how quickly the volume fades and quickly it pauses but otherwise there's not a lot to tweak.  I use XFCE and xscreensaver, I have not recently tested it on KDE or Gnome so the "screen locked" detection very well might need updating to work on other environments.

# Utility programs

## mpdctl.sh 

This script is what I use to control mpd via hotkeys in my desktop, such as adjust rating, next, prev, volume, etc.  It has some minimal output but is primarily just to control it with shortcuts

## sticker.sh 

This script get or sets a sticker for the current song.  Mostly superflous, but keeping it around for now.

## autopause.sh

This script will automatically pause mpd playback when media is playing in something that supports MPRIS, such as Chrome, Firefox, vlc, etc.  You must specify the apps it's looking for in the script and run it on startup and it should pause music when media is playing and resume playing when the media is paused or stopped.  You can also manually resume music while media is playing and it will continue to play after the media has paused or stopped.

This script can only detect if media is playing if the app show up in playerctl -l while playing media. If they do not, it will not detect them.  mplayer does not seem to support MPRIS at all, mpv has a lua helper script to add MPRIS support to it, vlc seems to have it out of the box.  Chrome and Firefox work without any modifications.  Note that Chrome shows up as Chromium.

If you need to add a new media player to the list, have it play media and run playerctl -l to see if it shows up and what the name is.

If you want to check for other conditions, such as if a specific program is running, or perhaps even time of day, you can add the logic to the extraMedia function in env.sh and set mediaplaying to 1 if you want mpd to pause.  This function is called after the main mediaApps list is looped over so you could also over-ride the mediaplaying variable here in certain cases if that is useful.

## genrestats.sh

Very simple script that will generate a list of the genres in your mpd library and sort them in descending order.

# Amarok migration

And one last script in the amarok directory can help you migrate your ratings from Amarok into the mpd sticker database by exporting the data from the Amarok database into a 'csv' (really, pipe separated) file, then the script parses that csv and has mpc add the data to the sticker database.

The biggest thing that you'll need to do here is once you export the Amarok database ratings you'll need to make sure the file path matches the one that mpd knows about.  It will very likely be a different top directory or two and probably is a pretty trivial replace-all operation, but they need to match exactly.

# Upstream issues that could simplify scripts

I have logged two issues upstream to mpc/mpd which could simplify these scripts quite a bit, if they both get resolved I will refactor the scripts to use them.  Ideally if both were implemented, the cache files would not be needed at all, we would simply ask mpc for a random file of a given query, be it all, or of a sticker type, etc.  Would also eliminate the need for nc and shuf.

 * [https://github.com/MusicPlayerDaemon/MPD/issues/759](https://github.com/MusicPlayerDaemon/MPD/issues/759)
   * Request to add the ability to query stickers directly in mpc rather than only through the API
   * If fixed could eliminate use of nc
   * Fix may be coming in mpd 22, scripts are being updated to not use nc if so
     * 22 does not appear to have fixed it, it's still on the roadmap
 * [https://github.com/MusicPlayerDaemon/mpc/issues/52](https://github.com/MusicPlayerDaemon/mpc/issues/52)
   * Request to add the ability to return a single random song from a find/search query to mpc
   * If fixed could eliminate cache files altogether
   * Closed by mpd devs; suggests piping into shuf
  
# References

 * [https://www.musicpd.org/doc/html/protocol.html](https://www.musicpd.org/doc/html/protocol.html)

# Create playlists for other devices

Since these scripts are having to maintain a cache of songs for each playlist, it's trivial to convert the cache file contents to use on other devices, such as my Android phone.  My music follows the same paths, starting at /sdcard/Music/ so for example I can do this:

`cat ~/.mpd-fourstars.txt | sed -e 's|file: |/sdcard/Music/|' >fourstars2.m3u`

And then send that to my phone, scan for playlists, and just works!  I'm sure there are other clients etc that can do this more directly, but I've never used those to send playlists or songs to my devices.

PS:  Yes, you can use any delimiter with sed, I'm using pipes above to avoid having to escape the forward slashes.

Caveat; keep in mind that the cache files are purely from ratings.  Any other exclusions that your playlists would do, such as excluding genres etc, would not be reflected in the cache files.

# TODO

 * Pair songs together, so that if song A is queued, B is also queued
   * More difficult; if B is queued, queue A in front of it
 * Possibly look into a last played kind of lookup to eliminate playing songs frequently, although would I have to write a sticker every time for this?