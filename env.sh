## queueloop.sh variables
playlist="threestars"
queueSize="14"
oldSongs="4"
sleepTime="1"

# Hostname of the mpd server, used by mpc  if mpcOptions is defined
# but also used by nc to get a list of songs to play from
host="localhost"
port="6600"

# If you use a password on mpd, you'll need to add it here in the format of
# password@ so that it passes --host=password@host to mpc
#password="mpd@"

# mpc options that need to be passed
mpcOptions="--host=${password}${host} --port=${port}"

# Paired songs

# If you want two songs to always play together, put them here in this list
# using in the full path (uri) of each song

# Use this type of format: paired="one#two|nextsongone#nextsongtwo"

paired="Rock/Alan Parsons Project/Eye In The Sky/01 Alan Parsons Project - Eye In The Sky - 01 - Sirius (instr).mp3#Rock/Alan Parsons Project/Eye In The Sky/02 Alan Parsons Project - Eye In The Sky - 02 - Eye In The Sky.mp3|\
Rock/Led Zeppelin/15 Box Set disc 1/02 Led Zeppelin - Box Set Disc One - 02 - Heartbreaker.mp3#Rock/Led Zeppelin/15 Box Set disc 1/03 Led Zeppelin - Box Set Disc One - 03 - Living Loving Maid (She's Just A Woman).mp3"

## autopause.sh options

# List of media apps to check in autopause.sh, separated by a space
# Note that they need co show up in playerctl -l while playing media.
# If you don't want to check for any media apps just set to whitespace
mediaApps="chromium firefox mpv vlc"

# This function is called from autopause.sh to do optional extra logic.
# Add any logic you like here to check for other conditions and set
# mediaplaying to 1 to pause MPD.
extraMedia () {

    # Example; checking to see if Tomb Raider is running
    tombraider=$(pgrep -fc tomb123.exe)
    if [ "${tombraider}" -gt 0 ]; then
        mediaplaying='1'
        # No need to test any more
        return
    fi

}