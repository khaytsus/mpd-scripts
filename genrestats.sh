#!/bin/bash

# Genre statistics
scriptDir="$(dirname "$(readlink -f "$0")")"
source ${scriptDir}/env.sh

mpc ${mpcOptions} listall -f '%genre%' | sort | uniq -c | sort -rn