#!/bin/bash

# Fade mpd's volume when the screensaver is active using ${cof}*x^2 until the volume
# approaches 0.  This formula has a fairly flat curve initially, so a very slow
# volume decay for the first few minutes, then towards the end the volume decays more
# rapidly.

# You can adjust how long this curve takes to complete, the smaller the coefficient, the
# longer it takes before the volume reaches 0 and we pause mpd.  Examples:
#   cof=.0001 and sleeptime=2 takes about 8 minutes.
#  cof=.00001 and sleeptime=2 takes about 10 minutes
# cof=.000002 and sleeptime=2 takes about 23 minutes
cof=".000002"

# Delay between loops, smaller delay and more loops means a smoother volume decay and
# less delay between screensaver exiting and music beginning
sleeptime="2"

# Action to do when resuming, next will start the next song, toggle
# would resume the song that was playing last
resumeaction="next"

# Temporary file to make sure we don't run multiple times
tmpfile=~/.mpdscreensaver

if [ "$1" == "-tmp" ]; then
  rm $tmpfile
fi

if [ -f $tmpfile ]; then
  echo "Already running, exiting..  -tmp to nuke file and continue.."
  exit
fi

touch $tmpfile

# Get our mpc optional arguments
scriptDir="$(dirname "$(readlink -f "$0")")"
if [ -e ${scriptDir}/env.sh ]; then
    source ${scriptDir}/env.sh
fi

debug=1
log ()
{
    string=$*
    dt=`date +%m-%d-%H:%M:%S`
    if [ $debug == 1 ]; then
        echo "${dt} - ${string}"
    fi
}

isScreenSaverBlanked ()
{
	# Check if GNOME is running
# Need to verify if this still works
	if [ $GNOME_DESKTOP_SESSION_ID ]; then
		isBlanked=$(dbus-send --reply-timeout=2000 --print-reply --dest=org.gnome.ScreenSaver /org/gnome/ScreenSaver org.gnome.ScreenSaver.GetActive 2>&1 | grep boolean | grep true)
		if [ -n "$isBlanked" ];then 
			return 1
		fi 
	fi

# This right for KDE still?
#	if [ $(dcop kdesktop KScreensaverIface isBlanked) = "true" ];then
#		return 1
#	fi 

# Check xscreenaver
	output=`xscreensaver-command -time | grep "locked since"`
	if [ "$output" != "" ]; then
		#log "XScreenSaver is locked.."
		return 1
	fi	

# If we can't determine info from screensavers, query dpms
# Probably is a better way to do this..
	state=`xset -display :0 q | grep "Monitor is" | sed -e 's/^[ ]*//g'`
	#log "Monitor state is [${state}]"
	if [ "$state" == "Monitor is in Standby" ];
	then
		log "Monitor is in standby"
		return 1
	fi
	if [ "$state" == "Monitor is in Suspend" ];
	then
		log "Monitor is in suspend"
		return 1
	fi
	if [ "$state" == "Monitor is Off" ];
	then
		log "Monitor is in off"
		return 1
	fi

	return 0
}

fadeMPD ()
{
	pause=1

	#origvol=`mpc ${mpcOptions} volume | cut -f 2 -d ":" | cut -f 1 -d "%"`
	origvol=`mpc ${mpcOptions} volume | grep -o -E '[0-9]+'`
	log "Entered fadeMPD, original volume was ${origvol}"

	newvol=${origvol}

	# Start loop at 1
	loop="1"
	time="0"

	while [ ${newvol} -gt 0 ]; do
		# Store our previous volume
		prevol=${newvol}
		# Calculate a value where we are on our volume ramp based on the current loop
		multiplier=`echo "(-${cof}*${loop}^2)+1" | bc`
		# Calculate a new volume, (...)/1 makes it an integer

		newvol=`echo "(${origvol} * ${multiplier})/1" | bc`
		# Make sure our volume is sane and isn't the same as it already is, adjust volume
		if [ ${newvol} -gt 0 ] && [ ${newvol} -ne ${prevol} ]; then
			# adjust mpd volume here of course
			mpc ${mpcOptions} -q volume ${newvol}
		fi
		((loop++))
		time=$((time + sleeptime))
		# Break out of our loop if the screensaver is unblanked
		isScreenSaverBlanked
			if [ $? = 0 ]; then
				log "Screensaver unblanked"
				pause=0
				# If this doesn't work, use a variable flag, but should work fine
				break
			fi
			#log "Sleeping ${sleeptime} (total time so far: ${time})"
		sleep ${sleeptime}
	done

	if [ $pause = 1 ]; then
		log "Fading complete, restoring for next unblank"
# If we pause manually during sleep, it'll UNPAUSE later, so make sure it's playing first
		play_status=`mpc ${mpcOptions} status | grep '\[playing\]\|\[paused\]' | cut -f 2 -d '[' | cut -f 1 -d ']'`
		if [ "$play_status" = "playing" ]; then
			log "Executing mpc pause"
			mpc ${mpcOptions} -q pause
		fi

		sleep 10
		log "Setting volume back to $origvol"
		mpc ${mpcOptions} -q volume $origvol
		sleep 2
	else
		log "Unblanked before pause; setting volume back to $origvol"
		mpc ${mpcOptions} -q volume $origvol
	fi
	log "Exiting fadeMPD"
}

sigterm1 ()
{
	log "Signal caught, restoring volume to $origvol and exiting"
	mpc ${mpcOptions} -q volume $origvol
	sleep 1
	rm $tmpfile
	exit
}

trap sigterm1 SIGTERM SIGINT

# Track if we are blanked or not, so we don't hit mpd constantly for no reason
blanked="0"

# Loop forever, checking screensaver status to see if we should drop into
# the fadeMPD loop
while [ true ];
do
	isScreenSaverBlanked
	if [ $? = 1 ]; then
		# If we were previously not blanked, evaluate if we should fadeMPD now
		if [ ${blanked} -eq 0 ]; then
			# We've entered screensaver mode, set blanked so we bypass this until unblanked
			# if we're not playing at this point
			blanked="1"
			log "Entered screensaver mode, should only see this once per cycle!"
			play_status=`mpc ${mpcOptions} status | grep '\[playing\]\|\[paused\]' | cut -f 2 -d '[' | cut -f 1 -d ']'`

			# If mpd is playing, start fading
			if [ "$play_status" = "playing" ]; then
				# We stay in this function until we fade out to 0
				fadeMPD
				log "Returned from fadeMPD"

				# Reset our blanked flag
				blanked="0"

				# Once fadeMPD exits, loop here until we're unblanked
				isScreenSaverBlanked
                while [ $? = 1 ];
                do
                    sleep ${sleeptime}
					isScreenSaverBlanked
                done

				# Get our current mpd status now that we've exited fadeMPD
				# and the screen is not blanked.
				play_status=`mpc ${mpcOptions} status | grep '\[playing\]\|\[paused\]' | cut -f 2 -d '[' | cut -f 1 -d ']'`
				log "Returned from fadeMPD and blank, status: $play_status"

				# We have came out of screensaver and MPD is paused
				if [ "$play_status" = "paused" ]; then
					### Workaround:  Can't set volume while mpd is stopped, so if we pause,
					### we need to do "mpc next" later to resume to not resume mid-song
					mpc ${mpcOptions} -q ${resumeaction}
					log "Executing ${resumeaction} from status: [${play_status}]"
				fi

				# We have came out of screensaver and MPD has stopped playing entirely
				if [ "$play_status" = "" ]; then
					mpc ${mpcOptions} -q play
					log "Starting playing from stopped"
				fi
			fi
		fi
	else
		#log "Setting blanked=0"
		blanked="0"
	fi
	# Sleep 5s until we test again
	sleep ${sleeptime}
done
