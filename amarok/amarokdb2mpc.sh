#!/bin/bash

# Run the following
# mysql DB -uUSER -pPASS -e "SELECT u.rpath, s.rating, s.playcount FROM urls u, statistics s WHERE s.url = u.id AND s.url = u.id INTO OUTFILE '/tmp/ratings.csv' FIELDS TERMINATED BY '|' OPTIONALLY ENCLOSED BY '\"' LINES TERMINATED BY '\n'"\

# Then copy the csv file generated to the path defined below

# MAKE SURE the file paths match what mpc shows, you can use mpc listall to see
# the exact format, it may vary from what Amarok had stored.  If so, you will
# need to update the 'csv' file before you run the script.  If you don't mpc will
# just complain about not finding a file in mpd, but of course won't store a
# rating that matches that song so you'll want to make it work right.

arg=$1

csvfile="${HOME}/ratings.csv"

if [ "${arg}" == "db" ]; then
  echo "getting csv file"

fi

while IFS= read -r line; do
    # Split the string on |
    set -f; IFS='|'
    set -- ${line}
    file=$1; rating=$2; playcount=$3
    set +f; unset IFS

    # Get the filename out of the quotes
    set -f; IFS='"'
    set -- ${file}
    uri=$2
    set +f; unset IFS

    mpc sticker "${uri}" set rating "${rating}"
    rc=$?
    if [ ${rc} != 0 ]; then
        echo "Error with ${uri} rating:[${rating}] playcount:[${playcount}]"
    fi
done < ${csvfile}